//
//  ViewController.h
//  CoreDataAssign
//
//  Created by Trevor Eckhardt on 7/9/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

//#import <UIKit/UIKit.h>
//#import "AppDelegate.h"
//#import <MessageUI/MessageUI.h>
//#import <MessageUI/MFMailComposeViewController.h>
//#import <MapKit/MapKit.h>
//
//@interface ViewController : UIViewController<MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate,
//MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate, MKMapViewDelegate>

//@property (nonatomic, strong) CLLocationManager* locationManager;
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, MKMapViewDelegate>
{
    AppDelegate* ad;
    NSManagedObject* object;
}

@end

