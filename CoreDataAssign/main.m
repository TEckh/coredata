//
//  main.m
//  CoreDataAssign
//
//  Created by Trevor Eckhardt on 7/9/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
