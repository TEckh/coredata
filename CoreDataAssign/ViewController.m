//
//  ViewController.m
//  CoreDataAssign
//
//  Created by Trevor Eckhardt on 7/9/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Getting our app delegate
    ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //Creating a person object
    NSManagedObjectContext *context = [ad managedObjectContext];
    
    NSManagedObject *person = [NSEntityDescription
                                insertNewObjectForEntityForName:@"Person"
                                inManagedObjectContext:context];
    [person setValue:@"Twilight Sparkle" forKey:@"name"];
    [person setValue:@"577 South 900 West, Salt Lake City Utah" forKey:@"address"];
    [person setValue:@"PraiseTheSun@gmail.com" forKey:@"email"];
    [person setValue:@"8013854320" forKey:@"phone"];
    
    NSManagedObject *person1 = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Person"
                                    inManagedObjectContext:context];
    [person1 setValue:@"Applejack" forKey:@"name"];
    [person1 setValue:@"107 North Mountain Road, Kaysville Utah" forKey:@"address"];
    [person1 setValue:@"FarmerPony@gmail.com" forKey:@"email"];
    [person1 setValue:@"4243765829" forKey:@"phone"];
    
    NSManagedObject *person2 = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Person"
                                    inManagedObjectContext:context];
    [person2 setValue:@"Fluttershy" forKey:@"name"];
    [person2 setValue:@"1060 South 900 West, Salt Lake City Utah" forKey:@"address"];
    [person2 setValue:@"TimidPegasus@gmail.com" forKey:@"email"];
    [person2 setValue:@"3580864927" forKey:@"phone"];
    
//    NSManagedObject *person = [NSEntityDescription
//                               insertNewObjectForEntityForName:@"Person"
//                               inManagedObjectContext:context];
//    [person setValue:@"Adam Hellewell" forKey:@"name"];
//    [person setValue:@"3848 Harrison Blvd, Ogden Utah" forKey:@"address"];
//    [person setValue:@"adamhellewell@weber.edu" forKey:@"email"];
//    [person setValue:@"8017250192" forKey:@"phone_number"];
    
    //Saving person object
    NSError *error;
    BOOL saveSucceeded = [context save:&error];
    if (!saveSucceeded) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    //Fetching our person objects
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    //Looping through and diplaying the persons name
    for (NSManagedObject *info in fetchedObjects) {
        NSLog(@"Name: %@", [info valueForKey:@"name"]);
        object = info;
        //Editing an object
        //        [info setValue:@"my name" forKey:@"name"];
        //
        //        if (![context save:&error]) {
        //            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        //        }
    }
    
    
    //Fetching all objects and printing and object
    //    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    //    for (NSManagedObject *info in fetchedObjects) {
    //        NSLog(@"Name: %@", [info valueForKey:@"name"]);
    //    }
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"topuch" forState:UIControlStateNormal];
    btn.frame = CGRectMake(100, 100, 100, 100);
    [btn addTarget:self action:@selector(selectImage:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    //Map code
//    MKMapView* map = [[MKMapView alloc] initWithFrame:self.view.bounds];
//    map.delegate = self;
//    map.showsUserLocation = YES;
//    [self.view addSubview:map];
    
//    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
//    [annotation setCoordinate:centerCoordinate];
//    [annotation setTitle:@[self.obj valueForKey:@"Name"]]; //You can set the subtitle too
//    [map addAnnotation:annotation];
    
//    self.locationManager = [[CLLocationManager alloc] init];
    // Use one or the other, not both. Depending on what you put in info.plist
//    [self.locationManager requestWhenInUseAuthorization];
    //        [self.locationManager requestAlwaysAuthorization];
}

-(void)selectImage:(id)sender {
    //Open maps app
    NSString *addressString = @"http://maps.apple.com/?q=1+Infinite+Loop,+Cupertino,+CA";
    NSURL *url = [NSURL URLWithString:addressString];
    [[UIApplication sharedApplication] openURL:url];
    
    //Email someone
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"My Subject"];
        [controller setMessageBody:@"Hello there." isHTML:NO];
        [controller setToRecipients:@[[object valueForKey:@"email"]]];
        [self presentViewController:controller animated:YES completion:^{
    
        }];
    
    
    //Text someone
        MFMessageComposeViewController *textController = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            textController.body = @"SMS message here";
            textController.recipients = [NSArray arrayWithObjects:@"8017250192", nil];
            textController.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:^{
    
            }];
        }
    
    
    //Call someone
        NSLog(@"%@", object);
        NSString *cleanedString = [[[object valueForKey:@"phone_number"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
    
    //Select image
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
        [self presentViewController:picker animated:YES completion:^{
    
        }];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:controller completion:^{
        
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    [self dismissViewControllerAnimated:picker completion:^{
        
    }];
    UIImageView* iv = [[UIImageView alloc]initWithImage:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    iv.frame = CGRectMake(200, 200, 100, 100);
    [self.view addSubview:iv];
    
    NSData* imageData = UIImageJPEGRepresentation(iv.image, 0.8);
    //Store image data in core data
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
